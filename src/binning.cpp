/*
 * binning.cpp
 *
 *  Created on: 06 Jul 2017
 *      Author: dylan
 */

#include "../headers/binning.h"

int Domain::generateBins()
{
	if(xmax <= xmin || ymax <= ymin){std::cout << "Invalid domain bounds." << std::endl; return 0;}
	if(nbinsx < 1 || nbinsy < 1){std::cout << "Invalid bin count." << std::endl; return 0;}

	float x = 0.0;
	float y = 0.0;
	bins = std::vector<std::vector<Bin> >(nbinsx);
	for(int i=0; i<nbinsx; i++) {
		bins[i] = std::vector<Bin>(nbinsy);
		for(int j=0; j<nbinsy; j++) {
			bins[i][j].bounds[0] = x;
			bins[i][j].bounds[1] = x+bin_width;
			bins[i][j].bounds[2] = y;
			bins[i][j].bounds[3] = y+bin_height;
			bins[i][j].av_v[0] = 0;
			bins[i][j].av_v[1] = 0;
			bins[i][j].av_v[2] = 0;
			bins[i][j].npart = 0;
			bins[i][j].v_mag = 0;
			y += bin_height;
		}
		y = 0.0;
		x += bin_width;
	}
	return 1;
}

int Domain::insertParticle(Particle* p)
{
	int i = floor((p->x[d1] - xmin)/bin_width);
	int j = floor((p->x[d2] - ymin)/bin_height);

	if(i<0 || i>=nbinsx || j<0 || j>=nbinsy)return 0;

	total_part++;
	bins[i][j].updateBin(p);

	return 1;
}

void Domain::getAverageVelocity(int i, int j, float vel_out[3])
{
	for(int d=0; d<3; d++)
		vel_out[d] = bins[i][j].av_v[d];
}

int Domain::getPopulation(int i, int j)
{
	return (int)(bins[i][j].npart);
}

float Domain::getPopulationFraction(int i, int j)
{
	return bins[i][j].npart/total_part;
}

float* Domain::getBounds(int i, int j) {
	return bins[i][j].bounds;
}

float Domain::getAverageSpeed(int i, int j)
{
	return bins[i][j].v_mag;
}










