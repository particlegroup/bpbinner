/*
 * cache.cpp
 *
 *  Created on: 04 Jul 2017
 *      Author: dylan
 */
#include <boost/filesystem.hpp>
#include "../headers/cache.h"
#include <string>

int Cache::loadFrame(unsigned int frame_num, CacheFrame* frame_out)
{
	FILE* infile;
	if(frame_num >= file_paths.size()){return 0;}
	std::string spath = file_paths[frame_num];
	infile = fopen(spath.c_str(), "rb");

	if(infile==NULL){return 0;}

	decodeHeader(frame_out, &infile);

	Particle p;

	while(decodeNextParticle(&p, &infile)) {
		frame_out->particles.push_back(Particle(p));
	}

	fclose(infile);
	return 1;
}

int Cache::decodeHeader(CacheFrame* frame, FILE** infile)
{
	int success = 1;
	for(unsigned int i=0; i<8; i++) {
		frame->header.title[i] = fgetc(*infile);
	}
	fread((unsigned int*)(&(frame->header.type)), 4, 1, *infile);
	fread((unsigned int*)(&(frame->header.totpoints)), 4, 1, *infile);
	fread((unsigned int*)(&(frame->header.dtypes)), 4, 1, *infile);

	return success;
}

int Cache::decodeNextParticle(Particle* p, FILE** infile)
{
	if(feof(*infile)){return 0;}

	fread((unsigned int*)(&(p->index)), sizeof(p->index), 1, *infile);

	for(int i=0; i<3; i++) {
		fread(&(p->x[i]), sizeof(p->x[i]), 1, *infile);
	}

	for(int i=0; i<3; i++) {
		fread(&(p->v[i]), sizeof(p->v[i]), 1, *infile);
	}

	return 1;
}

int Cache::createFileList()
{
	namespace fs = boost::filesystem;
	fs::path p(folder);
	fs::directory_iterator it(p);
	while (it != fs::directory_iterator()){
		std::string tmp = it->path().string();
		int fnum = getFrameNumber(tmp);
		if(end == 0 && start == 0) {
			file_paths.push_back(tmp);
		}
		else if(fnum >= start && fnum <= end) {
			file_paths.push_back(tmp);
		}
		it++;
	}
	std::sort(file_paths.begin(), file_paths.end());
	num_files = file_paths.size();
	return 1;
}

int Cache::getFrameNumber(std::string spath)
{
	int uscore_pos = spath.rfind("_");
	std::string sframe = spath.substr(uscore_pos-6, 6);
	return std::atoi(sframe.c_str());
}



