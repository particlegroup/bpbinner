//============================================================================
// Name        : BlenderParticleReader.cpp
// Author      : Dylan Blakemore
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include "../headers/cache.h"
#include "../headers/cacheframe.h"
#include "../headers/binning.h"
#include "../csv/csv.h"

int main() {
	std::string folder;
	std::cout << "Enter the path to the folder containing the baked files.\n";
	std::cin >> folder;


	int dims[2] = {0,1};
	int start = 200;
	int end = 400;
	int percentloaded = 0;
	int nbinsx = 260;
	int nbinsy = 78;
	Cache cache(folder, start, end);
	Domain domain = Domain(0.2, 1.5, 0.01, 0.4, dims, nbinsx,nbinsy);

	std::cout << "Total files loaded: " << cache.num_files << std::endl;
	std::cout << "Binning baked data..." << std::endl;

	for(int i=0; i<cache.num_files; i++) {
		CacheFrame frame = CacheFrame();
		cache.loadFrame(i, &frame);
		for(unsigned int j=0; j<frame.particles.size(); j++) {
			domain.insertParticle(&(frame.particles[j]));
		}
		float fracdone = (float)(i)/(float)(cache.num_files);
		if(100.0*fracdone >= percentloaded+5) {
			percentloaded+=5;
			std::cout << "Percent loaded: " << percentloaded << std::endl;
		}
	}
	std::cout << "Loading complete, writing to disk..." << std::endl;

	std::string csvpath;
	std::cout << "Enter the path to the output .csv file.\n";
	std::cin >> csvpath;

	CSVFile out(csvpath);
	out.open('w');
	for(int i=0; i<nbinsx; i++) {
		for(int j=0; j<nbinsy; j++) {
			if(domain.getPopulation(i,j) < 10)
				continue;
			for(int b=0; b<4; b++) {
				out.write(domain.getBounds(i,j)[b]);
			}
			float av_v[3];
			domain.getAverageVelocity(i, j, av_v);
			for(int d=0; d<3; d++)
				out.write(av_v[d]);
			out.write(domain.getAverageSpeed(i,j));
			out.write(domain.getPopulation(i,j));
			out.newline();
		}
	}
	out.close();
	std::cout << "Writing complete." << std::endl;

	return 0;
}
