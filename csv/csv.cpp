/*
 * csv.cpp
 *
 *  Created on: 06 Jul 2017
 *      Author: dylan
 */

#include "csv.h"

int CSVFile::write(float f)
{
	if(mode == 'n')return 0;

	if(first) {
		outfile << f;
		first = 0;
	} else {
		outfile << "," << f;
	}
	return 1;
}

int CSVFile::newline()
{
	if(mode == 'n')return 0;

	outfile << "\n";
	first = 1;

	return 1;
}

std::vector<std::vector<float> > CSVFile::readAll()
{
	std::vector<std::vector<float> > arr;

	while(infile.good()) {
		std::string line;
		getline(infile, line);
		boost::trim(line);
		if(line.empty())break;
		std::vector<float> tmp_v;
		std::string tmp_s;
		std::stringstream stream(line);
		while(getline(stream, tmp_s, ',')) {
			tmp_v.push_back(atof(tmp_s.c_str()));
		}
		arr.push_back(tmp_v);
	}
	return arr;
}


