/*
 * csv.h
 *
 *  Created on: 06 Jul 2017
 *      Author: dylan
 */

#ifndef CSV_H_
#define CSV_H_

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <boost/algorithm/string.hpp>

class CSVFile
{
public:
	CSVFile(const CSVFile& in)
	{
		fpath = in.fpath;
		delim = in.delim;
		mode = 'n';
		first = 1;
	}
	CSVFile(std::string path, char delimiter=',')
	{
		fpath = path;
		delim = delimiter;
		mode = 'n';
		first = 1;
	}

	int open(char m)
	{
		if(mode != 'n') {
			return 0;
		}
		if(m == 'w') {
			mode = m;
			outfile.open(fpath.c_str());
			return 1;
		}
		if(m == 'r') {
			mode = m;
			infile.open(fpath.c_str());
			return 1;
		}
		return 0;
	}

	void close()
	{
		if(mode == 'w')outfile.close();
		if(mode == 'r')infile.close();
	}

	~CSVFile()
	{
		if(mode == 'w')outfile.close();
		if(mode == 'r')infile.close();
	}

	int write(float f);
	int newline();
	std::vector<std::vector<float> > readAll();

private:
	char delim;
	char mode;
	int first;
	std::string fpath;
	std::ofstream outfile;
	std::ifstream infile;
};



#endif /* CSV_H_ */
