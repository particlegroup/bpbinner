/*
 * cache.h
 *
 *  Created on: 04 Jul 2017
 *      Author: dylan
 */

#ifndef CACHE_H_
#define CACHE_H_

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <cstring>
#include <stdio.h>
#include <boost/filesystem.hpp>

#include "cacheframe.h"

class Cache
{
public:
	Cache(std::string fname)
	{
		folder = fname;
		start = 0;
		end = 0;
		createFileList();
	}

	Cache(std::string fname, int fstart, int fend)
	{
		folder = fname;
		start = fstart;
		end = fend;
		createFileList();
	}

	int loadFrame(unsigned int frame_num, CacheFrame* frame_out);
	int num_files;

private:
	std::string folder;
	std::vector<std::string> file_paths;
	int start;
	int end;

	int decodeHeader(CacheFrame* frame, FILE** infile);
	int decodeNextParticle(Particle* p, FILE** infile);
	int createFileList();
	int getFrameNumber(std::string spath);
};



#endif /* CACHE_H_ */
