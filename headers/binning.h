/*
 * binning.h
 *
 *  Created on: 06 Jul 2017
 *      Author: dylan
 */

#ifndef BINNING_H_
#define BINNING_H_

#include <vector>
#include <iostream>
#include <math.h>
#include "cacheframe.h"

struct Bin
{
	float av_v[3];
	float v_mag;
	float npart;
	float bounds[4];
	void updateBin(Particle* p)
	{
		float vx = p->v[0];
		float vy = p->v[1];
		float vz = p->v[2];
		float v = sqrt(vx*vx + vy*vy + vz*vz);
		v_mag = (v_mag*npart + v)/(npart+1);
		for(int i=0; i<3; i++)
			av_v[i] = (av_v[i]*(npart) + p->v[i])/(npart+1);
		npart++;
	}
};

class Domain
{
public:
	Domain(float x1, float x2, float y1, float y2, int dims[2], int nx=10, int ny=10)
	{
		xmin = x1;
		xmax = x2;
		ymin = y1;
		ymax = y2;
		nbinsx = nx;
		nbinsy = ny;
		d1 = dims[0]; d2 = dims[1];
		bin_width  = (xmax - xmin)/(float)nbinsx;
		bin_height = (ymax - ymin)/(float)nbinsy;
		generateBins();
	}

	int insertParticle(Particle* p);
	void getAverageVelocity(int i, int j, float vel_out[3]);
	float getAverageSpeed(int i, int j);
	int getPopulation(int i, int j);
	float getPopulationFraction(int i, int j);
	float* getBounds(int i, int j);

private:
	int generateBins();
	float xmin, xmax, ymin, ymax, bin_width, bin_height;
	int nbinsx, nbinsy, d1, d2;
	double total_part;
	std::vector<std::vector<Bin> > bins;
};




#endif /* BINNING_H_ */
