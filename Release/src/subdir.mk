################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/BlenderParticleReader.cpp \
../src/binning.cpp \
../src/cache.cpp 

OBJS += \
./src/BlenderParticleReader.o \
./src/binning.o \
./src/cache.o 

CPP_DEPS += \
./src/BlenderParticleReader.d \
./src/binning.d \
./src/cache.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


